<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//auth
Route::group([
    'middleware' => 'api',
], function () {
    Route::post('login', 'App\Http\Controllers\AuthController@login');
    Route::post('register', 'App\Http\Controllers\AuthController@register');
    //Route::post('logout', 'AuthController@logout');
    //Route::post('refresh', 'AuthController@refresh');
});

//verify email
Route::group([
    'prefix' => 'email',
], function () {
    Route::get('verify/{id}', 'App\Http\Controllers\VerificationController@verify')->name('verification.verify');
    Route::get('resend', 'App\Http\Controllers\VerificationController@resend')->name('verification.resend');
});

//reset password
Route::group([
    'prefix' => 'password',
], function () {
    Route::post('forgot', 'App\Http\Controllers\ForgotPasswordController@forgot');
    Route::post('reset', 'App\Http\Controllers\ResetPasswordController@reset');
});

//events
Route::prefix('events')->middleware('auth:api')->group(function () {

    Route::get('/', 'App\Http\Controllers\EventController@index');
    Route::post('/', 'App\Http\Controllers\EventController@create');

    Route::prefix('{event}')->group(function () {
        Route::get('/', 'App\Http\Controllers\EventController@show');
        Route::patch('/', 'App\Http\Controllers\EventController@update');
        Route::delete('/', 'App\Http\Controllers\EventController@destroy');
    });
});

//user
Route::prefix('users')->middleware('auth:api')->group(function () {
    Route::prefix('/me')->group(function () {
        Route::get('/', 'App\Http\Controllers\AuthController@me')->middleware('verified');
        //Route::patch('/', 'App\Http\Controllers\UserController@selfUpdate');
    });
    Route::get('/', 'App\Http\Controllers\UserController@index');

    Route::prefix('{user}')->group(function () {
        Route::get('/', 'App\Http\Controllers\UserController@show');
        Route::patch('/', 'App\Http\Controllers\UserController@update');
    });

});

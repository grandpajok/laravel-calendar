<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Response\JsonResponse;

class VerificationController extends Controller
{
    public function verify($user_id, Request $request) {
        if (!$request->hasValidSignature()) {
            return new JSONResponse(['message' => trans('auth.failed')], 401);
        }

        $user = User::findOrFail($user_id);

        if (!$user->hasVerifiedEmail()) {
            $user->markEmailAsVerified();
        }

        return redirect()->to('/');
    }

    public function resend() {
        if (auth()->user()->hasVerifiedEmail()) {
            return new JSONResponse(['message' => 'Email already veryfied!'], 400);
        }

        auth()->user()->sendEmailVerificationNotification();

        return new JSONResponse(['message' => 'Email verification link sent on your email id'], 200);
    }
}

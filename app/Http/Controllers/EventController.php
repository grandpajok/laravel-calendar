<?php

namespace App\Http\Controllers;

use Validator;
use App\Models\Event;
use App\Http\Response\JsonResponse;
use App\Http\Response\ErrorResponse;
use App\Http\Resources\EventResource;
use App\Http\Resources\EventResourceCollection;
use App\Models\SearchModels\EventSearch;
use App\Http\Requests\EventSearchRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param App\Http\Requests\EventSearchRequest
     * @return \Illuminate\Http\Response
     */
    public function index(EventSearchRequest $request)
    {
        if (Gate::denies('add-other-users-to-event')) {
            $request->users = auth()->user()->id;
        }

        $query = Event::query();
        $query = EventSearch::search($query, $request);
        $events = $query->get();

        return new JsonResponse(['data' => new EventResourceCollection($events), 'message' => trans('http.success')]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $rules = Event::rules_create();
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return new JsonResponse(['errors' => $validator->errors(), 'message' => trans('http.wrong_data')], 422);
        }

        DB::beginTransaction();
        try {
            $event = Event::create(array_merge(
                $validator->validated(),
                ['user_create' => auth()->user()->id]
            ));

            if (!$event->save()) {
                return new JsonResponse(['message' => trans('event.already_exist_3')], 400);
            }

            $event->users()->sync($request->users);

        } catch (Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage(), ['DB']);
            return new ErrorResponse($e->getMessage(), 500);
        }
        DB::commit();

        return new JsonResponse(['data' => new EventResource($event), 'message' => trans('http.create')]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        if (!$event->exists) {
            return new ErrorResponse(trans('http.resource_not_found'));
        }

        if (Gate::denies('update-event', $event)) {
            return new JsonResponse(['message' => trans('auth.no_permissions')], 401);
        }

        return new JsonResponse(['data' => new EventResource($event), 'message' => trans('http.success')]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Event $event)
    {
        if (Gate::denies('update-event', $event)) {
            return new JsonResponse(['message' => trans('auth.no_permissions')], 401);
        }

        $rules = Event::rules_update();
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return new JsonResponse(['errors' => $validator->errors(), 'message' => trans('http.wrong_data')], 422);
        }

        DB::beginTransaction();
        try {
            $event->fill(
                $validator->validated()
            );
            if ($request->users) {
                $event->users()->sync($request->users);
            }
            $event->update();
        } catch (Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage(), ['DB']);
            return new ErrorResponse($e->getMessage(), 500);
        }
        DB::commit();

        return new JsonResponse(['data' => new EventResource($event), 'message' => trans('http.create')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        if (Gate::denies('update-event', $event)) {
            return new JsonResponse(['message' => trans('auth.no_permissions')], 401);
        }

        DB::beginTransaction();
        try {
            $event->delete();
        } catch (Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage(), ['DB']);
            return new ErrorResponse($e->getMessage(), 500);
        }
        DB::commit();

        return new JsonResponse(['message' => trans('http.success')]);
    }
}

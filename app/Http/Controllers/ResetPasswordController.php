<?php

namespace App\Http\Controllers;

use App\Http\Requests\ResetPasswordRequest;
use Illuminate\Support\Facades\Password;
use App\Http\Response\JsonResponse;

class ResetPasswordController extends Controller
{
    /**
     * Reset user password by given token.
     *
     * @param App\Http\Requests\ResetPasswordRequest
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function reset(ResetPasswordRequest $request) {
        $reset_password_status = Password::reset($request->validated(), function ($user, $password) {
            $user->password = $password;
            $user->save();
        });

        if ($reset_password_status == Password::INVALID_TOKEN) {
            return new JsonResponse(['message' => trans('auth.invalid_reset_token')], 401);
        }

        return new JSONResponse(['message' => trans('auth.success_reset')], 200);
    }
}

<?php

namespace App\Http\Controllers;

use Validator;
use App\Models\User;
use App\Models\SearchModels\UserSearch;
use Illuminate\Http\Request;
use App\Http\Response\JsonResponse;
use App\Http\Response\ErrorResponse;
use App\Http\Resources\UserResource;
use App\Http\Resources\UserResourceCollection;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    /**
     * Display a listing of the user resource collection.
     *
     * @param Illuminate\Http\Request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = User::query();
        $query = UserSearch::search($query, $request);
        $users = $query->get();

        return new JsonResponse(['data' => new UserResourceCollection($users), 'message' => trans('http.success')]);
    }

    /**
     * Display the user resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        if (!$user->exists) {
            return new ErrorResponse(trans('http.resource_not_found'));
        }

        return new JsonResponse(['data' => new UserResource($user), 'message' => trans('http.success')]);
    }

    /**
     * Update the specified user model.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        if (Gate::denies('update-user', $user)) {
            return new JsonResponse(['message' => trans('auth.no_permissions')], 401);
        }

        $rules = User::rules_update();
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return new JsonResponse(['errors' => $validator->errors(), 'message' => trans('http.wrong_data')], 422);
        }

        if ($request->role) {
            if (Gate::denies('change-user-role')) {
                return new JsonResponse(['message' => trans('auth.no_permissions_change_role')], 401);
            }
        }

        DB::beginTransaction();
        try {
            $user->fill(
                $validator->validated(),
            );
            $user->update();
        } catch (Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage(), ['DB']);
            return new ErrorResponse($e->getMessage(), 500);
        }
        DB::commit();

        return new JsonResponse(['data' => new UserResource($user), 'message' => trans('http.patch')]);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use App\Http\Response\JsonResponse;
use Validator;

class ForgotPasswordController extends Controller
{
    /**
     * Send reset password link if user exists by given email.
     *
     * @param Illuminate\Http\Request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function forgot(Request $request)
    {
        $validator = Validator::make($request->only('email'), ['email' => 'required|email|exists:users,email']);

        if ($validator->fails()){
            return new JsonResponse(['errors' => $validator->errors(), 'message' => trans('auth.incorrect_creds')], 422);
        }

        Password::sendResetLink($validator->validated());

        return new JsonResponse(['message' => trans('auth.success_forgot')], 200);
    }
}

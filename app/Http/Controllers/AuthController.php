<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Response\JsonResponse;
use App\Http\Response\TokenResponse;
use App\Http\Resources\UserResource;
use Validator;
use App\Models\User;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @param Illuminate\Http\Request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->only('email', 'password'), [
            'email' => 'required|email',
            'password' => 'required|string|min:6',
        ]);

        if ($validator->fails()) {
            return new JsonResponse(['errors' => $validator->errors(), 'message' => trans('auth.failed')], 422);
        }

        if (! $token = auth()->attempt($validator->validated())) {
            return new JsonResponse(['errors' => $validator->errors(), 'message' => trans('auth.failed')], 401);
        }

        return new TokenResponse($token);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @param Illuminate\Http\Request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $rules = User::rules_create();
        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()){
            return new JsonResponse(['errors' => $validator->errors(), 'message' => trans('auth.failed')], 422);
        }

        $user = User::create(
            $validator->validated(),
        );

        auth()->login($user);
        $user->sendEmailVerificationNotification();

        return new JsonResponse(['message' => trans('auth.success_registration')], 200);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        if (auth()->user()) {
            return new JsonResponse(['data' => new UserResource(auth()->user()), 'message' => trans('http.success')]);
        }
        return new JsonResponse(['message' => trans('auth.unauth')], 401);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use App\Http\Response\JsonResponse;
use Illuminate\Http\Exceptions\HttpResponseException;

class EventSearchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'sometimes|string',
            'description' => 'sometimes|string',
            'startDate' => 'sometimes|required|date',
            'endDate' => 'sometimes|required|date',
            'userCreate' => Gate::allows('add-other-users-to-event') ? "sometimes|required|numeric|exists:users,id" : "sometimes|required|numeric|in:" . auth()->user()->id,
            "users"  => Gate::allows('add-other-users-to-event') ? "sometimes|required|numeric|exists:users,id" : "sometimes|required|numeric|in:" . auth()->user()->id,
        ];

        return $rules;
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(new JsonResponse(['errors' => $validator->errors(), 'message' => trans('http.wrong_data')], 422));
    }
}

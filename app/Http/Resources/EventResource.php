<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\UserResourceCollection;
use App\Http\Resources\UserResource;

class EventResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $users = $this->users()->get();
        $userCreate = $this->userCreate()->find($this->user_create);

        return [
            'id' => $this->id,
            'description' => $this->description,
            'name' => $this->name,
            'date' => $this->date,
            'users' => self::prepareUsers($users),
            'userCreate' => self::prepareUserCreate($userCreate)
        ];
    }

    public static function prepareUsers($users)
    {
        return new UserResourceCollection($users);
    }

    public static function prepareUserCreate($user)
    {
        return new UserResource($user);
    }
}

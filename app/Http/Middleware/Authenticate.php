<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Closure;
use Tymon\JWTAuth\Exceptions;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Response\JsonResponse;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Middleware\BaseMiddleware;

class Authenticate
{
    /**
     * Check if JWT token expired
     *
     * Try to make and send new token
     *
     * @param Request $request Incoming request
     * @param Closure $next    Next responses
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);
        if ($request->method() != 'OPTIONS') {
            try {
                try {
                    $user = JWTAuth::setRequest($request)->parseToken()->authenticate();
                    if (!$user) {
                        return new JsonResponse(['message' => trans('auth.unauth')], 401);
                    }
                }
                catch (Exceptions\TokenBlacklistedException $e) {
                    return new JsonResponse(['message' => $e->getMessage()], 401);
                }
            } catch (Exceptions\TokenExpiredException $e) {
                $token = JWTAuth::refresh(JWTAuth::getToken());
                // send the refreshed token back to the client
                header("Authorization: Bearer $token");
                $user = JWTAuth::setToken($token)->toUser();
            } catch (Exceptions\JWTException $e) {
                return new JsonResponse(['errors' => $e->getMessage(), 'message' => trans('auth.unauth')], 401);
            }

            auth()->login($user);
        }

        return $response;
    }
}

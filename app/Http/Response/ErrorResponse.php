<?php

namespace App\Http\Response;

use Illuminate\Http\JsonResponse as HttpJsonResponse;
use InvalidArgumentException;

class ErrorResponse extends HttpJsonResponse
{
    /**
     * Constructor.
     *
     * @param mixed $data    Incoming data
     * @param int   $status  Status code
     * @param array $headers Headers
     * @param int   $options Options
     *
     * @return void
     */
    public function __construct($data=null, $status=400, $headers=[], $options=0)
    {
        $this->encodingOptions = $options;
        parent::__construct($data, $status, $headers);
    }

    /**
     * Sets the msg to be sent as JSON.
     *
     * @param mixed $msg Incoming message
     *
     * @return $this
     *
     * @throws \InvalidArgumentException
     */
    public function setData($msg="")
    {
        $response = [
            'status' => $this->isOk(),
            'code' => $this->statusCode,
            'message' => $msg
        ];
        $this->data = json_encode($response);

        if (! $this->hasValidJson(json_last_error())) {
            throw new InvalidArgumentException(json_last_error_msg());
        }

        return $this->update();
    }
}

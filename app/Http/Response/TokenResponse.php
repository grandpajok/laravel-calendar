<?php

namespace App\Http\Response;

use Illuminate\Http\JsonResponse as HttpJsonResponse;
use InvalidArgumentException;

class TokenResponse extends HttpJsonResponse
{
    /**
     * Sets the data to be sent as JSON.
     *
     * @param mixed $token Token
     *
     * @return $this
     *
     * @throws \InvalidArgumentException
     */
    public function setData($token="")
    {
        $response = [
            'status'  => $this->isOk(),
            'code' => $this->statusCode,
        ];
        if ($token) {
            $response['data'] = [
                'jwt' => [
                    'token' => $token,
                    'type' => 'Bearer'
                ]
            ];
        }

        $this->data = json_encode($response);
        $this->header('Authorization: Bearer', $token);
        if (! $this->hasValidJson(json_last_error())) {
            throw new InvalidArgumentException(json_last_error_msg());
        }

        return $this->update();
    }
}

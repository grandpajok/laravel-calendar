<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use App\Models\User;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('update-event', function ($user, $event) {
            if ($user->role === User::ADMIN_ROLE) {
                return true;
            } else {
                return $user->id === $event->user_create;
            }
        });

        Gate::define('add-other-users-to-event', function ($user) {
            return $user->role === User::ADMIN_ROLE;
        });

        Gate::define('update-user', function ($user, $user_update) {
            if ($user->role === User::ADMIN_ROLE) {
                return true;
            } else {
                return $user->id === $user_update->id;
            }
        });

        Gate::define('change-user-role', function ($user) {
            return $user->role === User::ADMIN_ROLE;
        });
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class EventUser extends Pivot
{
    use HasFactory;

    protected $table = 'event_user';

    /**
     * Relation user table
     *
     * @return HasOne
     */
    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

    /**
     * Relation user table
     *
     * @return HasOne
     */
    public function event()
    {
        return $this->hasOne('App\Models\Event', 'id', 'event_id');
    }
}

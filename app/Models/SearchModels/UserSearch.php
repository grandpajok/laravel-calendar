<?php

namespace App\Models\SearchModels;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

class UserSearch
{
    public const DEFAULT_ORDER_BY = 'id';
    public const DEFAULT_SORT_DIRECTION = 'asc';
    public const DEFAULT_LIMIT = 15;

    /**
     * Build query for user
     *
     * @param Builder $query
     *
     * @param Request $request
     *
     * @return Builder $query
     */
    public static function search(Builder $query, Request $request)
    {
        if ($request->name) {
            $query->orWhere('name', 'like', "%{$request->name}%");
        }

        $orderBy = $request->sort ?? self::DEFAULT_ORDER_BY;
        $orderDirection = $request->order ?? self::DEFAULT_SORT_DIRECTION;
        $limit = $request->limit ?? self::DEFAULT_LIMIT;
        $offset = $request->offset ?? 0;

        $query->orderBy($orderBy, $orderDirection)
            ->offset($offset)
            ->limit($limit);

        return $query;
    }
}

<?php

namespace App\Models\SearchModels;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

class EventSearch
{
    public const DEFAULT_ORDER_BY = 'events.id';
    public const DEFAULT_SORT_DIRECTION = 'asc';
    public const DEFAULT_LIMIT = 15;

    /**
     * Build query for user
     *
     * @param Builder $query
     *
     * @param Request $request
     *
     * @return Builder $query
     */
    public static function search(Builder $query, Request $request)
    {
        $query->select('events.*');

        if ($request->name) {
            $query->orWhere('name', 'like', "%{$request->name}%");
        }

        if ($request->description) {
            $query->orWhere('description', 'like', "%{$request->name}%");
        }

        if ($request->userCreate) {
            $query->where('user_create', '=', "$request->userCreate");
        }

        if ($request->users) {
            $query
                ->join('event_user', 'event_user.event_id', '=', 'events.id')
                ->join('users', 'event_user.user_id', '=', 'users.id')
                ->where('users.id', '=', "$request->users");
        }

        if ($request->startDate && $request->endDate) {
            $query->whereBetween("date", [$request->startDate, $request->endDate]);
        }

        $orderBy = $request->sort ? 'events.' . $request->sort : self::DEFAULT_ORDER_BY;
        $orderDirection = $request->order ?? self::DEFAULT_SORT_DIRECTION;
        $limit = $request->limit ?? self::DEFAULT_LIMIT;
        $offset = $request->offset ?? 0;

        $query->orderBy($orderBy, $orderDirection)
            ->offset($offset)
            ->limit($limit);

        return $query;
    }
}

<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Validation\Rule;

class User extends Authenticatable implements JWTSubject, MustVerifyEmail
{
    use HasFactory, Notifiable;

    /**
     * Admin user role
     *
     * @var integer
     */
    public const ADMIN_ROLE = 1;

    /**
     * Default user role
     *
     * @var integer
     */
    public const MANAGER_ROLE = 2;

    /**
     * Roles array
     *
     * @var array
     */
    public const ROLES_ARRAY = [
        self::ADMIN_ROLE,
        self::MANAGER_ROLE
    ];

    /**
     * Validator creating rules array
     *
     * @var array
     */
    public static function rules_create()
    {
      return [
            'email' => 'sometimes|required|email|unique:users',
            'login' => 'sometimes|required|unique:users',
            'password' => 'required|string|confirmed|min:6',
            'name' => 'required|string',
        ];
    }

    /**
     * Validator creating rules array
     *
     * @var array
     */
    public static function rules_update()
    {
      return [
            'email' => 'sometimes|email|unique:users',
            'login' => 'sometimes|unique:users',
            'password' => 'string|confirmed|min:6',
            'role' => 'sometimes|required|numeric', Rule::in(Rule::in(self::ROLES_ARRAY))
        ];
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'login',
        'password',
        'name',
        'role'
    ];

    /**
     * Default value of attributes
     *
     * @var array
     */
    protected $attributes = [
        'role' => self::MANAGER_ROLE
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
    */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
    * Return a key value array, containing any custom claims to be added to the JWT.
    *
    * @return array
    */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * Save Hashed password
     */
    public function setPasswordAttribute($value) {
        $this->attributes['password'] = Hash::make($value);
    }

    /**
     * Relation to file table
     *
     * @return BelongsToMany
     */
    public function events()
    {
        return $this->belongsToMany('App\Models\Event');
    }
}

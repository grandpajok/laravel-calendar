<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Gate;

class Event extends Model
{
    use HasFactory;

    /**
     * Validator creating rules array
     *
     * @var array
     */
    public static function rules_create()
    {
      return [
            'date' => 'required|date|after:today',
            'description' => 'sometimes|required|string',
            'name' => 'required|string|string',
            "users"    => "required|array",
            "users.*"  => Gate::allows('add-other-users-to-event') ? "required|numeric|distinct|exists:users,id" : "required|numeric|in:" . auth()->user()->id,
        ];
    }

    /**
     * Validator updating rules array
     *
     * @var array
     */
    public static function rules_update()
    {
      return [
            'date' => 'sometimes|required|date|after:today',
            'description' => 'sometimes|required|string',
            'name' => 'sometimes|required|string',
            "users"    => "sometimes|required|array",
            "users.*"  => Gate::allows('add-other-users-to-event') ? "sometimes|required|numeric|distinct|exists:users,id" : "sometimes|required|numeric|in:" . auth()->user()->id,
        ];
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date',
        'name',
        'description',
        'user_create'
    ];

    /**
     * Relation users table
     *
     * @return BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\Models\User', 'event_user', 'event_id', 'user_id')->using('App\Models\EventUser');
    }

    /**
     * Relation user table
     *
     * @return HasOne
     */
    public function userCreate()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_create');
    }
}

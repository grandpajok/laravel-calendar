<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Response\ErrorResponse;
use App\Http\Response\JSONResponse;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        $msg  = $exception->getMessage();
        $code = 404;

        if ($exception instanceof ModelNotFoundException) {
            return new ErrorResponse($msg, $code);
        }

        if ($exception instanceof TokenBlacklistedException) {
            return new JSONResponse(['message' => $exception->getMessage()], 401);
        }

        if ($exception instanceof HttpException && $exception->getMessage() === 'Your email address is not verified.') {
            return new JSONResponse(['message' => $exception->getMessage()], 403);
        }

        return parent::render($request, $exception);
    }
}

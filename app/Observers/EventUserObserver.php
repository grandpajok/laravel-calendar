<?php

namespace App\Observers;

use App\Models\EventUser;
use App\Notifications\AddedToEventNotification;
use App\Notifications\RemovedFromEventNotification;
use Illuminate\Notifications\Notification;

class EventUserObserver
{
    /**
     * Handle the event "created" event.
     *
     * @param  \App\Models\EventUser  $relation
     * @return void
     */
    public function created(EventUser $relation)
    {
        $user = $relation->user()->find($relation->user_id);
        $event = $relation->event()->find($relation->event_id);
        $user->notify(new AddedToEventNotification($event));
    }

    /**
     * Handle the event "updated" event.
     *
     * @param  \App\Models\EventUser $event
     * @return void
     */
    public function updated(EventUser $event)
    {
        //
    }

    /**
     * Handle the event "deleted" event.
     *
     * @param  \App\Models\EventUser  $relation
     * @return void
     */
    public function deleted(EventUser $relation)
    {
        $user = $relation->user()->find($relation->user_id);
        $event = $relation->event()->find($relation->event_id);
        $user->notify(new RemovedFromEventNotification($event));
    }

    /**
     * Handle the event "restored" event.
     *
     * @param  \App\Models\EventUser  $event
     * @return void
     */
    public function restored(EventUser $event)
    {
        //
    }

    /**
     * Handle the event "force deleted" event.
     *
     * @param  \App\Models\EventUser  $event
     * @return void
     */
    public function forceDeleted(EventUser $event)
    {
        //
    }
}

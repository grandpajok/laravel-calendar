<?php

namespace App\Observers;

use App\Models\Event;
use App\Http\Response\JsonResponse;
use App\Notifications\EventCreatedNotification;
use App\Notifications\EventDeletedNotification;
use Illuminate\Notifications\Notification;

class EventObserver
{
    /**
     * Handle the event "created" event.
     *
     * @param  \App\Models\Event  $event
     * @return void
     */
    public function created(Event $event)
    {
        $user = $event->userCreate()->find($event->user_create);
        $user->notify(new EventCreatedNotification($event));
    }

    /**
     * Handle the event "updated" event.
     *
     * @param  \App\Models\Event  $event
     * @return void
     */
    public function updated(Event $event)
    {
        //
    }

    /**
     * Handle the event "deleted" event.
     *
     * @param  \App\Models\Event  $event
     * @return void
     */
    public function deleted(Event $event)
    {
        $user = $event->userCreate()->find($event->user_create);
        $user->notify(new EventDeletedNotification($event));
    }

    /**
     * Handle the event "restored" event.
     *
     * @param  \App\Models\Event  $event
     * @return void
     */
    public function restored(Event $event)
    {
        //
    }

    /**
     * Handle the event "force deleted" event.
     *
     * @param  \App\Models\Event  $event
     * @return void
     */
    public function forceDeleted(Event $event)
    {
        //
    }

    /**
     * Disallow creating Event if there are 3 already exist at defined date
     *
     * @param  \App\Models\Event  $event
     * @return void
     */
    public function creating(Event $event)
    {
        $date = new \DateTime($event->date);
        $date = $date->format('Y-m-d');
        $count = Event::whereDate('date', '=', "$date")->count();
        if ($count >= 3) {
            return false;
        }
    }
}
